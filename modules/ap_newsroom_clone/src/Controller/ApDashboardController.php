<?php

namespace Drupal\ap_newsroom_clone\Controller;

use Drupal\ap_newsroom\ApNewsroomContent;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\ap_newsroom_clone\ApDashboardService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * Class ApDashboardController.
 *
 * @package Drupal\ap_newsroom_clone\Controller
 */
class ApDashboardController extends ControllerBase {

  /**
   * Variable used for loading Ap newsroom service.
   *
   * @var \Drupal\ap_newsroom_clone\ApDashboardService
   */
  protected $apDashboardService;

  /**
   * Ap Newsroom content service.
   *
   * @var \Drupal\ap_newsroom\ApNewsroomContent
   */
  protected $apNewsroomContent;

  /**
   * Constructs a NodeController object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   Passing an Instance of logger factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\ap_newsroom_clone\ApDashboardService $apDashboardService
   *   Ap dashboard service.
   * @param \Drupal\ap_newsroom\ApNewsroomContent $apNewsroomContent
   *   Ap newsroom service.
   */
  public function __construct(
    LoggerChannelFactory $logger_factory,
    MessengerInterface $messenger,
    ApDashboardService $apDashboardService,
    ApNewsroomContent $apNewsroomContent
  ) {
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
    $this->apDashboardService = $apDashboardService;
    $this->apNewsroomContent = $apNewsroomContent;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('ap_newsroom_clone.ap_newsroom_service'),
      $container->get('ap_newsroom.ap_newsroom_content_service')
    );
  }

  /**
   * Lists Feeds.
   *
   * @return array
   *   Feed list render array.
   *
   * @throws \exception
   */
  public function listFeeds() {

    if ($this->apDashboardService->isFeedChecked()) {
      $decodedJsonFeedData = $this->apNewsroomContent->feed();
    }
    else {
      $param = [];
      $page_size = $this->apDashboardService->getPageSize();
      if ($page_size) {
        $param['page_size'] = $page_size;
      }
      $decodedJsonFeedData = $this->apNewsroomContent->search($param);
    }
    $form = $this->formBuilder()
      ->getForm('Drupal\ap_newsroom_clone\Form\ApFeedsSearchForm', $decodedJsonFeedData);
    return $form;
  }

  /**
   * Method for updating data upon pagination.
   *
   * @param string $qt
   *   Query string of next/previous page.
   * @param string $page_num
   *   Page number/seq of next/previous page.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Return Ajax command
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function updateData($qt, $page_num) {
    if ($this->apDashboardService->isFeedChecked()) {
      $param = [
        'qt' => $qt,
        'seq' => $page_num,
      ];
      $decodedJsonFeedData = $this->apNewsroomContent->feed($param);
    }
    else {
      $param = [
        'qt' => $qt,
        'page' => $page_num,
      ];
      $decodedJsonFeedData = $this->apNewsroomContent->search($param);
    }

    $table = $this->apDashboardService->getTable($decodedJsonFeedData);
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#ap-news-table-list', $table));
    return $response;
  }

}
