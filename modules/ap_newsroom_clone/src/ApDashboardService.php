<?php

namespace Drupal\ap_newsroom_clone;

use Psr\Http\Message\UriInterface;
use Drupal\ap_newsroom\ApNewsroomContent;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ApDashboardService.
 *
 * @package Drupal\ap_newsroom_clone
 */
class ApDashboardService {

  use StringTranslationTrait;

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Request stack service object.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Content service.
   *
   * @var \Drupal\ap_newsroom\ApNewsroomContent
   */
  protected $apNewsroomContent;

  /**
   * Client factory service.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * Date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * SearchAndFeedHandler constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   Logger factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Drupal messenger service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\ap_newsroom\ApNewsroomContent $apNewsroomContent
   *   Ap newsroom content service.
   * @param \Drupal\Core\Http\ClientFactory $httpClientFactory
   *   HTTP client factory service.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   Date formatter service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    LoggerChannelFactory $logger_factory,
    MessengerInterface $messenger,
    RequestStack $requestStack,
    EntityTypeManagerInterface $entityTypeManager,
    ApNewsroomContent $apNewsroomContent,
    ClientFactory $httpClientFactory,
    DateFormatter $dateFormatter
  ) {
    $this->configFactory = $configFactory;
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
    $this->requestStack = $requestStack;
    $this->entityTypeManager = $entityTypeManager;
    $this->apNewsroomContent = $apNewsroomContent;
    $this->httpClientFactory = $httpClientFactory;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * D.I.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container interface object.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('entity_type.manager'),
      $container->get('ap_newsroom.ap_newsroom_content_service'),
      $container->get('http_client_factory'),
      $container->get('date.formatter')
    );
  }

  /**
   * Page size.
   *
   * @return string
   *   Return page size.
   */
  public function getPageSize() {
    $config = $this->configFactory->getEditable('ap_newsroom.base_config');
    return $config->get('ap_dashboard_page_size');
  }

  /**
   * Prepare node entity for cloning.
   *
   * @param string $entity_type
   *   Entity type to be clone.
   *
   * @return \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|null
   *   Return mapped node object.
   *
   * @throws \exception
   */
  public function prepareNodeForClone($entity_type) {

    $item_id = $this->getItemId();
    if ($item_id) {
      $singleContentResponse = $this->apNewsroomContent->getContentById($item_id);
      return $this->mapApDataToNode($entity_type, $singleContentResponse);
    }
    return NULL;
  }

  /**
   * Get itemID from url.
   *
   * @return mixed
   *   Return item id from url.
   *
   * @throws \exception
   */
  public function getItemId() {
    $item_id = $this->requestStack->getCurrentRequest()->query->get('item_id');
    if (!$item_id) {
      throw new \exception('Item id is not valid.');
    }
    return $this->requestStack->getCurrentRequest()->query->get('item_id');
  }

  /**
   * Map node field.
   *
   * @param string $entity_type
   *   Entity type to be clone.
   * @param array $singleContentResponse
   *   Single content response to be clone.
   *
   * @return \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface
   *   Return mapped node object.
   */
  public function mapApDataToNode($entity_type, array $singleContentResponse) {
    // Prepare Node.
    $node_data = [
      'type' => $entity_type,
    ];
    $node = '';
    try {
      $node = Node::create($node_data);
    }
    catch (\Exception $exception) {
      watchdog_exception('ap_newsroom_clone', $exception);
    }

    $config = $this->configFactory->getEditable('ap_newsroom_clone.field_mapping');

    // Get all the configured entity.
    $node_list = $config->get('node');
    foreach ($node_list[$entity_type]['fields'] as $field) {
      $this->resolveFieldTypeMapping($node, $field, $singleContentResponse);
    }
    return $node;
  }

  /**
   * Field type resolver.
   *
   * @param object $entity
   *   Entity for mapping.
   * @param array $field
   *   Field details to be mapped.
   * @param array $singleContentResponse
   *   Single content response to be clone.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function resolveFieldTypeMapping(&$entity, array $field, array $singleContentResponse) {
    switch ($field['type']) {
      case 'text':
        $this->mapTextTypeField($entity, $field, $singleContentResponse);
        break;

      case 'image':
        $this->mapImageField($entity, $field, $singleContentResponse);
        break;

      case 'nitf':
        $this->mapNitfField($entity, $field, $singleContentResponse);
        break;

      case 'paragraphs':
        $this->mapParagraphsType($entity, $field, $singleContentResponse);
        break;
    }
  }

  /**
   * Map text type field.
   *
   * @param object $entity
   *   Entity for mapping.
   * @param array $field
   *   Field details to be mapped.
   * @param array $item_data
   *   Data need to map with field.
   */
  public function mapTextTypeField(&$entity, array $field, array $item_data) {
    $field_name = $field['id'];
    $mapping_index = explode('.', $field['path']);
    // Loop for value.
    $field_value = $item_data['data'];
    for ($i = 0; $i < count($mapping_index); $i++) {
      if (isset($field_value[$mapping_index[$i]])) {
        $field_value = $field_value[$mapping_index[$i]];
      }
    }
    if (is_string($field_value) && $entity->hasField($field_name)) {
      $entity->$field_name->appendItem($field_value);
    }
  }

  /**
   * Map image type field.
   *
   * @param object $entity
   *   Entity for mapping.
   * @param array $field
   *   Field details to be mapped.
   * @param array $item_data
   *   Data need to map with field.
   *
   * @throws \exception
   */
  public function mapImageField(&$entity, array $field, array $item_data) {
    $field_name = $field['id'];
    $is_multiple = $field['multiple'];
    if (isset($item_data['data']['item']['associations'])) {
      $media_index = $item_data['data']['item']['associations'];
      foreach ($media_index as $media) {
        if ($media['type'] == 'picture') {
          $media_id = $media['altids']['itemid'];
          $media_data = $this->apNewsroomContent->getContentById($media_id);
          if ($media_data) {
            $file_data = $this->saveFile($media_data, $this->apNewsroomContent
              ->getApNewsroomApi()->getApiKey());
          }
          if (!empty($file_data) && $entity->hasField($field_name)) {
            $entity->$field_name->appendItem($file_data);
            if (!$is_multiple) {
              break;
            }
          }
        }
      }
    }
  }

  /**
   * Save file for image type.
   *
   * @param array $media_data
   *   Media details.
   * @param string $api_key
   *   AP newsroom api key.
   *
   * @return array
   *   Return image fid and other details.
   */
  public function saveFile(array $media_data, $api_key) {
    if (isset($media_data['data']['item']['renditions']['main'])) {
      $title = $media_data['data']['item']['title'];
      $alt_text = $media_data['data']['item']['headline'];
      if (isset($media_data['data']['item']['altids']['itemid'])) {
        $item_id = $media_data['data']['item']['altids']['itemid'];
        $uri = $media_data['data']['item']['renditions']['main']['href'];
        $image_url = $uri . '&apikey=' . $api_key;
        $actual_uri = NULL;

        // Get redirected url.
        $this->httpClientFactory->fromOptions([
          'allow_redirects' => [
            'on_redirect' => function (
              RequestInterface $request,
              ResponseInterface $response,
              UriInterface $uri
            ) use (&$actual_uri) {
              $actual_uri = (string) $uri;
            },
          ],
        ])->get($image_url);
        $redirected_url = $actual_uri . '&apikey=' . $api_key;
        $data = file_get_contents($redirected_url);
        $file = file_save_data($data, "public://$item_id.jpeg", FileSystemInterface::EXISTS_REPLACE);
        return [
          'target_id' => $file->id(),
          'alt' => $alt_text,
          'title' => $title,
        ];
      }
    }
    return [];
  }

  /**
   * Map Body from XML ap newsroom.
   *
   * @param object $entity
   *   Entity for mapping.
   * @param array $field
   *   Field details to be mapped.
   * @param array $item_data
   *   Data need to map with field.
   *
   * @throws \exception
   */
  public function mapNitfField(&$entity, array $field, array $item_data) {
    $field_name = $field['id'];
    if (isset($item_data['data']['item']['renditions']['nitf'])) {
      $href = $item_data['data']['item']['renditions']['nitf']['href'];
      $xml_data = $this->apNewsroomContent->getNitfByUrl($href);
      // Parse xml string.
      $xml_data = simplexml_load_string($xml_data);
      $body = $xml_data->body->{'body.content'}->asXML();
      if ($body && $entity->hasField($field_name)) {
        $entity->$field_name->appendItem($body);
      }
    }
  }

  /**
   * Map paragraphs type field.
   *
   * @param object $entity
   *   Entity for mapping.
   * @param array $field
   *   Field details to be mapped.
   * @param array $item_data
   *   Data need to map with field.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function mapParagraphsType(&$entity, array $field, array $item_data) {
    $field_name = $field['id'];
    foreach ($field['paragraph_list'] as $para_type => $para_fields) {
      $paragraph = $this->entityTypeManager
        ->getStorage('paragraph')
        ->create(['type' => $para_type]);
      foreach ($para_fields['fields'] as $para_field) {
        $this->resolveFieldTypeMapping($paragraph, $para_field, $item_data);
      }
      if (!empty($paragraph) && $entity->hasField($field_name)) {
        $entity->$field_name->appendItem($paragraph);
      }
    }
  }

  /**
   * Generate table structure for listing.
   *
   * @param array $decodedJsonFeedData
   *   Decode response from AP newsroom.
   *
   * @return array
   *   Return table render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function getTable(array $decodedJsonFeedData) {
    $header = [
      'headline' => $this->t('Headline'),
      'item_id' => $this->t('Item Id'),
      'version_created' => $this->t('Version Created'),
      'clone' => $this->t('Clone'),
    ];

    $rows = [];
    if (isset($decodedJsonFeedData['data']['items'])) {
      $items = $decodedJsonFeedData['data']['items'];
      foreach ($items as $key => $item) {
        $item = $item['item'];
        $clone_buttons = $this->getCloneButton($item['altids']['itemid']);
        $rows[$key] = [
          'headline' => [
            'data' => isset($item['headline']) ? $item['headline'] : '',
          ],
          'item_id' => [
            'data' => isset($item['altids']['itemid']) ? $item['headline'] : '',
          ],
          'version_created' => [
            'data' => isset($item['versioncreated']) ?
            $this->dateFormatter->format(strtotime($item['versioncreated']), 'custom', 'm/d/Y h:ia') : '',
          ],
          'clone' => [
            'data' => !empty($clone_buttons) ? $clone_buttons : '',
          ],
        ];
      }
    }

    return [
      '#type' => 'table',
      '#prefix' => '<div id="ap-news-table-list" class="table-feed-list">',
      '#suffix' => '</div>',
      '#header' => $header,
      '#rows' => $rows,
      '#footer' => $this->getFooter($decodedJsonFeedData),
      '#empty' => $this->t('No result found.'),
    ];
  }

  /**
   * Get clone dropbutton.
   *
   * @param string $item_id
   *   Item id.
   *
   * @return string[]|null
   *   Return clone button render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function getCloneButton($item_id) {
    $mapping_config = $this->configFactory->getEditable('ap_newsroom_clone.field_mapping');
    $node_mapping = $mapping_config->get('node');
    $clone_button = ['#type' => 'dropbutton'];
    if (!empty($node_mapping)) {
      foreach ($node_mapping as $type => $node) {
        $clone_title = $this->entityTypeManager
          ->getStorage('node_type')
          ->load($type)
          ->label();
        $clone_button['#links'][$type] = [
          'title' => $clone_title,
          'url' => Url::fromRoute('ap_newsroom_clone.clone_content', [
            'entity_type' => $type,
          ], [
            'query' => ['item_id' => $item_id],
            'attributes' => ['target' => '_blank'],
          ]),
        ];
      }
      return $clone_button;
    }
    return NULL;
  }

  /**
   * Get footer for table.
   *
   * @param array $decodedJsonFeedData
   *   Array response from AP newsroom api.
   *
   * @return array
   *   Return footer for list.
   */
  public function getFooter(array $decodedJsonFeedData) {
    $footer = [];
    if (isset($decodedJsonFeedData['data']['previous_page'])) {
      $parsed_url_previous_page = UrlHelper::parse($decodedJsonFeedData['data']['previous_page']);
      $footer['data'][] = $this->getPagerLinks($parsed_url_previous_page, '‹ Previous');
    }

    if (!$this->isFeedChecked()) {
      $footer['data'][] = $this->t('Page:') . $decodedJsonFeedData['data']['current_page'];
    }

    if (isset($decodedJsonFeedData['data']['next_page'])) {
      if ($this->isFeedChecked()) {
        $parsed_url_next_page = UrlHelper::parse($decodedJsonFeedData['data']['next_page']);
        $footer['data'][] = $this->getPagerLinks($parsed_url_next_page, 'More ›');
        return $footer;
      }
      $parsed_url_next_page = UrlHelper::parse($decodedJsonFeedData['data']['next_page']);
      $footer['data'][] = $this->getPagerLinks($parsed_url_next_page, 'Next ›');
    }
    return $footer;
  }

  /**
   * Generate pager for listing.
   *
   * @param array $param
   *   Parameter For pager.
   * @param string $label
   *   Label of pager link.
   *
   * @return \Drupal\Core\Link
   *   Return links for pager.
   */
  public function getPagerLinks(array $param, $label) {
    $qt = $param['query']['qt'];
    if ($this->isFeedChecked()) {
      $seq = $param['query']['seq'];
      $pager_link = Link::createFromRoute($label, 'ap_newsroom_clone.pager', [
        'qt' => $qt,
        'page_num' => $seq,
      ], [
        'attributes' => ['class' => 'use-ajax'],
      ]);
    }
    else {
      $page = $param['query']['page'];
      $pager_link = Link::createFromRoute($label, 'ap_newsroom_clone.pager', [
        'qt' => $qt,
        'page_num' => $page,
      ], [
        'attributes' => ['class' => 'use-ajax'],
      ]);
    }
    return $pager_link;
  }

  /**
   * Check if feed api in use.
   *
   * @return bool
   *   Return true is feed checked.
   */
  public function isFeedChecked() {
    $base_config = $this->configFactory->getEditable('ap_newsroom.base_config');
    if ($base_config->get('use_feed')) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

}
