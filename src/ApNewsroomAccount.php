<?php

namespace Drupal\ap_newsroom;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ApNewsroomAccount.
 */
class ApNewsroomAccount {

  /**
   * Api type.
   */
  const API_TYPE = "account";

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * ApNewsroomApi.
   *
   * @var ApNewsroomApi
   */
  protected $apNewsroomApi;

  /**
   * SearchAndFeedHandler constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   Logger factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   * @param ApNewsroomApi $apNewsroomApi
   *   ApNewsroomApi service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    LoggerChannelFactory $logger_factory,
    MessengerInterface $messenger,
    ApNewsroomApi $apNewsroomApi
  ) {
    $this->configFactory = $configFactory;
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
    $this->apNewsroomApi = $apNewsroomApi;
  }

  /**
   * D.I.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('ap_newsroom.ap_newsroom_api_service')
    );
  }

  /**
   * Retrieve your available account endpoints.
   *
   * @return array
   *   Return account endpoints.
   *
   * @throws \exception
   */
  public function getAccountEndpoints() {
    return $this->getAccountDetails('', []);
  }

  /**
   * Get account details from Ap newsroom.
   *
   * @param string $api_endpoint
   *   Api endpoints.
   * @param array $options
   *   Parameter array to use in api.
   *
   * @return array
   *   Return Account details.
   *
   * @throws \exception
   */
  public function getAccountDetails($api_endpoint, array $options) {
    $url = $this->getApNewsroomApi()
      ->generateApiUrl(self::API_TYPE, $api_endpoint, $options);
    return Json::decode($this->getApNewsroomApi()->sendRequest($url));
  }

  /**
   * Get ApNewsroomApi.
   *
   * @return ApNewsroomApi
   *   Return ApNewsroomApi service.
   */
  public function getApNewsroomApi() {
    return $this->apNewsroomApi;
  }

  /**
   * Retrieve your Followed Topics.
   *
   * @param array $param
   *   Parameter array to use in api.
   *
   * @return array
   *   Return topics for account.
   *
   * @throws \exception
   */
  public function getFollowedTopics(array $param = []) {
    $options = [
      'query' => $param,
    ];
    $api_endpoint = 'followedtopics';
    return $this->getAccountDetails($api_endpoint, $options);
  }

  /**
   * Retrieve your entitlements and associated meter information.
   *
   * @param array $param
   *   Parameter array to use in api.
   *
   * @return array
   *   Return account plan.
   *
   * @throws \exception
   */
  public function getAccountPlan(array $param = []) {
    $options = [
      'query' => $param,
    ];
    $api_endpoint = 'plans';
    return $this->getAccountDetails($api_endpoint, $options);
  }

  /**
   * A list of content items downloaded by you or your salesperson.
   *
   * @param array $param
   *   Parameter array to use in api.
   *
   * @return array
   *   Return Download history for account.
   *
   * @throws \exception
   */
  public function getDownloadHistory(array $param = []) {
    // Important:
    // The requested date range may not exceed 60 days.
    // The download information is available for the last 365 days only.
    $options = [
      'query' => $param,
    ];
    $api_endpoint = 'downloads';
    return $this->getAccountDetails($api_endpoint, $options);
  }

  /**
   * Returns the your account’s request limits for the various endpoints.
   *
   * @return array
   *   Return Account quotas.
   *
   * @throws \exception
   */
  public function getAccountQuotas() {
    $api_endpoint = 'quotas';
    return $this->getAccountDetails($api_endpoint, []);
  }

}
