<?php

namespace Drupal\ap_newsroom;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ApNewsroomContent.
 */
class ApNewsroomContent {

  /**
   * Api type.
   */
  const API_TYPE = "content";

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * ApNewsroomApi service.
   *
   * @var ApNewsroomApi
   */
  protected $apNewsroomApi;

  /**
   * SearchAndFeedHandler constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   Logger factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   * @param ApNewsroomApi $apNewsroomApi
   *   ApNewsroomApi service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    LoggerChannelFactory $logger_factory,
    MessengerInterface $messenger,
    ApNewsroomApi $apNewsroomApi
  ) {
    $this->configFactory = $configFactory;
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
    $this->apNewsroomApi = $apNewsroomApi;
  }

  /**
   * D.I.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('ap_newsroom.ap_newsroom_api_service')
    );
  }

  /**
   * Search content with search API.
   *
   * @param array $param
   *   Check Ap newsroom developer portal for parameters list.
   *
   * @return array
   *   Return search response from newsroom.
   *
   * @throws \exception
   */
  public function search(array $param = []) {
    $options = [
      'query' => $param,
    ];
    $api_endpoint = 'search';
    return $this->getContent($api_endpoint, $options);
  }

  /**
   * Get content from Ap newsroom.
   *
   * @param string $api_endpoint
   *   Endpoint for API.
   * @param array $options
   *   Parameter array to use in api.
   *
   * @return array
   *   Return response from Ap newsroom.
   *
   * @throws \exception
   */
  public function getContent($api_endpoint, array $options) {
    $url = $this->getApNewsroomApi()
      ->generateApiUrl(self::API_TYPE, $api_endpoint, $options);
    return Json::decode($this->getApNewsroomApi()->sendRequest($url));
  }

  /**
   * Get ApNewsroomApi.
   *
   * @return ApNewsroomApi
   *   Return ApNewsroomApi service.
   */
  public function getApNewsroomApi() {
    return $this->apNewsroomApi;
  }

  /**
   * Get feed from ap newsroom.
   *
   * @param array $param
   *   Check Ap newsroom developer portal for parameters list.
   *
   * @return array
   *   Return feed from AP newsroom.
   *
   * @throws \exception
   */
  public function feed(array $param = []) {
    $options = [
      'query' => $param,
    ];
    $api_endpoint = 'feed';
    return $this->getContent($api_endpoint, $options);
  }

  /**
   * Retrieve a list of available RSS XML feeds entitled to your plan.
   *
   * @return array
   *   Return rss.
   *
   * @throws \exception
   */
  public function rss() {
    $api_endpoint = 'rss';
    return $this->getContent($api_endpoint, []);
  }

  /**
   * Receive a RSS feed of latest AP Content for a Product (RSS) Id.
   *
   * @param string $rss_id
   *   Rss id.
   * @param array $param
   *   Check Ap newsroom developer portal for parameters list.
   *
   * @return array
   *   Return rss from AP newsroom.
   *
   * @throws \exception
   */
  public function rssByRssId($rss_id, array $param = []) {
    if (!$rss_id) {
      throw new exception('Rss id cannot be null.');
    }
    $options = [
      'query' => $param,
    ];
    $api_endpoint = 'rss/' . $rss_id;
    return $this->getContent($api_endpoint, $options);
  }

  /**
   * Receive a feed of contentitem object to your organization’s OnDemand queue.
   *
   * @param array $param
   *   Check Ap newsroom developer portal for parameters list.
   *
   * @return array
   *   Return feed on demand.
   *
   * @throws \exception
   */
  public function getFeedOnDemand(array $param = []) {
    $options = [
      'query' => $param,
    ];
    $api_endpoint = 'ondemand';
    return $this->getContent($api_endpoint, $options);
  }

  /**
   * Get next page content while search and feed.
   *
   * @param string $next_page_url
   *   Next page Url.
   *
   * @return bool|string
   *   Return next page content.
   *
   * @throws \exception
   */
  public function nextPage($next_page_url) {
    if (!$next_page_url) {
      throw new exception('Next page url cannot be null.');
    }
    $next_page_url = $next_page_url . '&apikey=' . $this->getApNewsroomApi()
      ->getApiKey();
    return $this->getApNewsroomApi()->sendRequest($next_page_url);
  }

  /**
   * Get previous page content while search and feed.
   *
   * @param string $previous_page_url
   *   Previous page url.
   *
   * @return bool|string
   *   Return previous page content.
   *
   * @throws \exception
   */
  public function previousPage($previous_page_url) {
    if (!$previous_page_url) {
      throw new exception('Previous page url cannot be null.');
    }
    $previous_page_url = $previous_page_url . '&apikey=' . $this->getApNewsroomApi()
      ->getApiKey();
    return $this->getApNewsroomApi()->sendRequest($previous_page_url);
  }

  /**
   * Get Nitf data in xml string by content id.
   *
   * @param string $item_id
   *   Item id.
   *
   * @return bool|string|null
   *   Return content by id.
   *
   * @throws \exception
   */
  public function getNitfByItemId($item_id) {
    if (!$item_id) {
      throw new exception('Item Id cannot be null.');
    }
    $content_item = $this->getContentById($item_id);
    if (isset($content_item['data']['item']['renditions']['nitf'])) {
      $nitf_url = $content_item['data']['item']['renditions']['nitf']['href'];
      return $this->getNitfByUrl($nitf_url);
    }
    return NULL;
  }

  /**
   * Fetch the contentitem object for a single piece of content by its Item ID.
   *
   * @param string $item_id
   *   Item id.
   * @param array $param
   *   Check Ap newsroom developer portal for parameters list.
   *
   * @return array
   *   Return content by id.
   *
   * @throws \exception
   */
  public function getContentById($item_id, array $param = []) {
    if (!$item_id) {
      throw new exception('Content id cannot be null.');
    }
    $options = [
      'query' => $param,
    ];
    $api_endpoint = $item_id;
    return $this->getContent($api_endpoint, $options);
  }

  /**
   * Get Nitf data in xml string by Nitf URL.
   *
   * @param string $nitf_url
   *   Nitf url.
   *
   * @return bool|string
   *   Return nitf url.
   *
   * @throws \exception
   */
  public function getNitfByUrl($nitf_url) {
    if (!$nitf_url) {
      throw new exception('Nitf url cannot be null.');
    }
    $nitf_url = $nitf_url . '&apikey=' . $this->getApNewsroomApi()->getApiKey();
    return $this->getApNewsroomApi()->sendRequest($nitf_url);
  }

}
