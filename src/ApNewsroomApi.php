<?php

namespace Drupal\ap_newsroom;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ApNewsroomApi.
 */
class ApNewsroomApi {

  use StringTranslationTrait;

  /**
   * Base URL for AP newsroom.
   */
  const AP_BASE_URL = "https://api.ap.org/media/";

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * API url not correct.
   *
   * @var string
   */
  protected $urlNotCorrectError = "URL you trying is not correct.
    Please verify URL or contact site admin for more details.";

  /**
   * Exception message for editor.
   *
   * @var string
   */
  protected $exceptionMsg = "Something has been wrong with AP News.
    Please contact Site Admin. AP News responded with status code :- @code";

  /**
   * Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * SearchAndFeedHandler constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   Logger factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger Service.
   * @param \GuzzleHttp\Client $httpClient
   *   Http client service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    LoggerChannelFactory $logger_factory,
    MessengerInterface $messenger,
    Client $httpClient
  ) {
    $this->configFactory = $configFactory;
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
    $this->httpClient = $httpClient;
  }

  /**
   * D.I.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('http_client')
    );
  }

  /**
   * Send request to URL.
   *
   * @param string $url
   *   Api full url.
   *
   * @return bool|string
   *   Return response from Ap newsroom.
   */
  public function sendRequest($url) {
    if (empty($url)) {
      $this->messenger->addError($this->t("Error while sending the request: @error", ["@error" => $this->urlNotCorrectError]));
      return FALSE;
    }

    try {
      $response = $this->httpClient->get($url);
      $data = $response->getBody();
      if (empty($data)) {
        return FALSE;
      }
      return $data->getContents();
    }
    catch (RequestException $e) {
      $exception_message = $this->t("@exception", ["@exception" => $this->exceptionMsg]);
      $this->messenger->addError($this->t("@exception", [
        "@code" => $e->getCode(),
        "@exception" => $exception_message->__toString(),
      ]));
      $variables = Error::decodeException($e);
      $this->loggerFactory->get('ap_newsroom_clone')
        ->error('%type: @message in %function (line %line of %file).', $variables);
      return FALSE;
    }
  }

  /**
   * Generate API url.
   *
   * @param string $api_type
   *   - Content.
   *   - Account.
   * @param string $api_endpoint
   *   - Visit ap newsroom developer portal for endpoints.
   *   https://api.ap.org/media/v/swagger/#/.
   * @param array $options
   *   Parameters to be use in api call.
   *
   * @return string
   *   Return API Url.
   *
   * @throws \exception
   */
  public function generateApiUrl($api_type, $api_endpoint, array $options) {
    // Add API key and item fragment.
    $options['query']['apikey'] = $this->getApiKey();
    $uri = $this->getBaseUrl($api_type) . $api_endpoint;
    $url = Url::fromUri($uri, $options);
    return $url->toUriString();
  }

  /**
   * Get API key for ap newsroom.
   *
   * @return array|mixed
   *   Return API key.
   *
   * @throws \exception
   */
  public function getApiKey() {
    $config = $this->configFactory->getEditable('ap_newsroom.base_config');
    $api_key = $config->get('ap_newsroom_key');
    if (!$api_key) {
      throw new \exception("API key not found.");
    }
    return $api_key;
  }

  /**
   * Get base URL with version and api type.
   *
   * @param string $api_type
   *   - Content.
   *   - Account.
   *
   * @return string
   *   Return base url.
   */
  public function getBaseUrl($api_type) {
    $base_url = self::AP_BASE_URL;
    $api_ver = $this->getApiVersion();
    if ($api_ver) {
      $base_url .= $api_ver . "/$api_type/";
    }
    else {
      $base_url .= "$api_type/";
    }
    return $base_url;
  }

  /**
   * Get version to be used for API.
   *
   * @return array|mixed|string
   *   Return API version.
   */
  public function getApiVersion() {
    $config = $this->configFactory->getEditable('ap_newsroom.base_config');
    $ver = $config->get('ap_newsroom_api_ver');
    if ($ver) {
      return $ver;
    }
    else {
      return 'v';
    }
  }

}
